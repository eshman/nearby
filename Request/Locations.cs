﻿using System.Globalization;

namespace GoogleApi.Entities.Places.Search.NearBy.Request
{
    /// Location.
    public class Location
    {    
        /// Latitude.    
        public double Latitude { get; set; }
  
        /// Longitude.
        public double Longitude { get; set; }

        /// Default Constructor.
        public Location()
        {

        }

        /// Contructor intializing a location using <paramref name="latitude"/> and <paramref name="longitude"/>.
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        public Location(double latitude, double longitude)
            : this()
        {
            this.Latitude = latitude;
            this.Longitude = longitude;
        }

        /// Overrdden ToString method for default conversion to Google compatible string.
        public override string ToString()
        {
            return this.Latitude.ToString(CultureInfo.InvariantCulture) + "," + this.Longitude.ToString(CultureInfo.InvariantCulture);
        }
    }
}