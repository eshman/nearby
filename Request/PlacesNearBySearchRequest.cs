﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace GoogleApi.Entities.Places.Search.NearBy.Request
{
 
    public class PlacesNearBySearchRequest : BasePlacesRequest
    {
 
        protected internal override string BaseUrl => base.BaseUrl + "nearbysearch/json";

        public virtual string Name { get; set; }

        public virtual string Keyword { get; set; }

        public virtual Location Location { get; set; }

        public virtual double? Radius { get; set; }

        /// pagetoken — Returns the next 20 results from a previously run search. 
        /// Setting a pagetoken parameter will execute a search with the same parameters 
        /// used previously — all parameters other than pagetoken will be ignored.
        public virtual string PageToken { get; set; }

        /// See <see cref="BasePlacesRequest.GetQueryStringParameters()"/>.
        /// <returns>The <see cref="IList{KeyValuePair}"/> collection.</returns>

        public override IList<KeyValuePair<string, string>> GetQueryStringParameters()
        {
            if (this.Location == null)
                throw new ArgumentException("Location is required");

            var parameters = base.GetQueryStringParameters();

            if (this.Radius.HasValue && (this.Radius > 50000 || this.Radius < 1))
                throw new ArgumentException("Radius must be greater than or equal to 1 and less than or equal to 50.000");

            if (!string.IsNullOrWhiteSpace(this.Name))
                parameters.Add("name", this.Name);

            if (!string.IsNullOrWhiteSpace(this.Keyword))
                parameters.Add("keyword", this.Keyword);

            parameters.Add("location", this.Location.ToString());

            if (this.Radius != null)
                parameters.Add("radius", this.Radius.Value.ToString(CultureInfo.InvariantCulture));

            if (!string.IsNullOrWhiteSpace(this.PageToken))
                parameters.Add("pagetoken", this.PageToken);

            return parameters;
        }
    }
}