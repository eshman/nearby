﻿using System.Collections.Generic;

namespace GoogleApi.Entities.Places.Search.NearBy.Response
{
    /// Places NearbySearch Response.
    public class PlacesNearbySearchResponse : BasePlacesResponse
    {
        /// Contains an array of places, with information about each. See Search Results for information about these results. 
        /// The Places API returns up to 20 establishment results per query
        [JsonProperty("results")]
        public virtual IEnumerable<NearByResult> Results { get; set; }

        [JsonProperty("next_page_token")]
        public virtual string NextPageToken { get; set; }
    }
}