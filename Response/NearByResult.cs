﻿using GoogleApi.Entities.Places.Search;
using Newtonsoft.Json;

namespace GoogleApi.Entities.Places.Search.NearBy.Response
{
    /// NearBy Result.
    public class NearByResult : BaseResult
    {
        /// Vicinity contains a feature name of a nearby location. 
        /// Often this feature refers to a street or neighborhood within the given results. 
        /// The vicinity property is only returned for a Nearby Search.
        [JsonProperty("vicinity")]
        public virtual string Vicinity { get; set; }
    }
}